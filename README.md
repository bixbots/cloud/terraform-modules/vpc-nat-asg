# vpc-nat-asg

## Summary

Provides a reusable module that deploys a VPC using [NAT instances](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_NAT_Instance.html) via EC2 in AWS. Using NAT instances vs NAT gateway provides significant cost savings especially when using spot instances. For more information, see [Comparison of NAT instances and NAT gateways](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-nat-comparison.html).

## Resources

In addition to the base VPC module:

* [vpc-base](https://gitlab.com/bixbots/cloud/terraform-modules/vpc-base)

This module creates the following resources:

* [aws_launch_configuration](https://www.terraform.io/docs/providers/aws/r/launch_configuration.html)
* [aws_autoscaling_group](https://www.terraform.io/docs/providers/aws/r/autoscaling_group.html)

## Cost

Use of this module will result in billable resources being launched in AWS. Before using this module, please familiarize yourself with the expenses associated with VPCs, EC2 instances and autoscaling groups.

* AWS VPC pricing information is available [here](https://aws.amazon.com/vpc/pricing/)
* AWS EC2 pricing information is available [here](https://aws.amazon.com/ec2/pricing/)

## Inputs

| Name                      | Description                                                                                                                                                                          | Type           | Default   | Required |
|:--------------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:---------------|:----------|:---------|
| application               | Name of the application the resource(s) will be a part of.                                                                                                                           | `string`       | `core`    | no       |
| environment               | Name of the environment the resource(s) will be a part of. This will be the simple name (i.e. short name) of the VPC.                                                                | `string`       | -         | yes      |
| role                      | Name of the role the resource(s) will be performing.                                                                                                                                 | `string`       | `network` | no       |
| zone_id                   | Specifies the Route53 Zone ID that this VPC belongs to.                                                                                                                              | `string`       | -         | yes      |
| cidr                      | CIDR of VPC. All subnets must be inside of this CIDR.                                                                                                                                | `string`       | -         | yes      |
| public_subnets            | A list of public subnets inside the VPC. The subnets will be placed into availability zones in they appear (first subnet goes into the first AZ, second goes into the second, etc).  | `list(string)` | -         | yes      |
| private_subnets           | A list of private subnets inside the VPC. The subnets will be placed into availability zones in they appear (first subnet goes into the first AZ, second goes into the second, etc). | `list(string)` | -         | yes      |
| data_subnets              | A list of data subnets inside the VPC. The subnets will be placed into availability zones in they appear (first subnet goes into the first AZ, second goes into the second, etc).    | `list(string)` | -         | yes      |
| azs                       | A list of availability zones in the region.                                                                                                                                          | `list(string)` | -         | yes      |
| enable_dns_hostnames      | Specifies whether to enable DNS hostnames in the VPC.                                                                                                                                | `bool`         | `true`    | no       |
| enable_dns_support        | Specifies whether to enable DNS support in the VPC.                                                                                                                                  | `bool`         | `true`    | no       |
| map_public_ip_on_launch   | Specifies whether to auto-assign public IPs when launching instances in public subnets.                                                                                              | `bool`         | `true`    | no       |
| enable_bastion_role       | Specifies whether to configure the NAT instances to also act as a jump server (aka bastion) to the internal (private and data) subnets.                                              | `bool`         | `false`   | no       |
| ami_id                    | The ID of the AMI to use for the NAT instances. If not specified, uses the latest version for AWS Linux 2.                                                                           | `string`       | -         | no       |
| instance_type             | The type of EC2 instance to use. If the instance type changes, a new launch configuration will be created and added to the ASG.                                                      | `string`       | `t3.nano` | no       |
| spot_price                | The maximum price to use for reserving spot instances. If omitted then spot instance will not be used.                                                                               | `string`       | -         | no       |
| key_name                  | The name of the SSH key pair that is used for administrator access to the NAT instance.                                                                                              | `string`       | -         | yes      |
| default_cooldown          | Amount of time, in seconds, after a scaling activity completes before another can begin.                                                                                             | `number`       | 300       | no       |
| health_check_grace_period | Amount of time, in seconds, after a instance comes into service before checking the health of the instance.                                                                          | `number`       | 300       | no       |
| enable_monitoring         | Whether to enable detailed monitoring with launch instances.                                                                                                                         | `bool`         | `false`   | no       |
| root_volume_size          | The size of the root volume in gigabytes.                                                                                                                                            | `number`       | 8         | no       |
| tags                      | Additional tags to attach to all resources created by this module.                                                                                                                   | `map(string)`  | -         | no       |

## Outputs

| Name                          | Description                                                                   |
|:------------------------------|:------------------------------------------------------------------------------|
| vpc_id                        | The ID of the VPC.                                                            |
| default_security_group_id     | The ID of the default security group.                                         |
| gateway_id                    | The ID of the internet gateway.                                               |
| public_subnets                | A list containing the ID of public subnets.                                   |
| private_subnets               | A list containing the ID of private subnets.                                  |
| data_subnets                  | A list containing the ID of data subnets.                                     |
| public_route_table_id         | The ID of the public routing table.                                           |
| private_route_table_ids       | A list containing the ID of private routing tables.                           |
| data_route_table_ids          | A list containing the ID of data routing tables.                              |
| nat_iam_role_id               | The ID of the NAT IAM role.                                                   |
| nat_iam_role_arn              | The ARN of the NAT IAM role.                                                  |
| nat_iam_role_name             | The name of the NAT IAM role.                                                 |
| nat_security_group_id         | The ID of the NAT security group.                                             |
| nat_security_group_arn        | The ARN of the NAT security group.                                            |
| nat_security_group_name       | The name of the NAT security group.                                           |
| nat_launch_configuration_id   | The ID of the NAT launch configuration.                                       |
| nat_launch_configuration_arn  | The ARN of the NAT launch configuration.                                      |
| nat_launch_configuration_name | The name of the NAT launch configuration.                                     |
| nat_autoscaling_group_ids     | The ID of the NAT autoscaling groups.                                         |
| nat_autoscaling_group_arns    | The ARN of the NAT autoscaling groups.                                        |
| nat_autoscaling_group_names   | The name of the NAT autoscaling groups.                                       |
| tags                          | A combined list of tags including input tags and tags created by this module. |

## Examples

### Simple Usage

```terraform
module "vpc_nat_asg" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/vpc-nat-asg.git?ref=v1"

  name            = "example"
  zone_id         = "todo"

  cidr            = "10.144.0.0/16"
  public_subnets  = ["10.144.0.0/21", "10.144.64.0/21", "10.144.128.0/21"]
  private_subnets = ["10.144.16.0/20", "10.144.80.0/20", "10.144.144.0/20"]
  data_subnets    = ["10.144.8.0/21", "10.144.72.0/21", "10.144.136.0/21"]
  azs             = ["us-east-1a", "us-east-1b", "us-east-1c"]

  instance_type = "t3.micro"
  spot_price    = "0.0052"
  key_name      = "example-keyname"

  tags = {
    Application  = "platform"
    Environment  = "example"
    Role         = "network"
    Lifespan     = "permanent"
  }
}
```

## Version History

* v1 - Initial Release
