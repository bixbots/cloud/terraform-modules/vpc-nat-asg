data "aws_subnet" "private" {
  count = length(module.vpc_base.private_subnets)
  id    = module.vpc_base.private_subnets[count.index]
}

data "aws_subnet" "data" {
  count = length(module.vpc_base.data_subnets)
  id    = module.vpc_base.data_subnets[count.index]
}

locals {
  internal_subnet_cidr_blocks = concat(data.aws_subnet.private.*.cidr_block, data.aws_subnet.data.*.cidr_block)
}

# egress

resource "aws_security_group_rule" "allow_http_to_everywhere" {
  security_group_id = module.launch_pad.primary_security_group_id

  type      = "egress"
  from_port = 80
  to_port   = 80
  protocol  = "tcp"

  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "allow_https_to_everywhere" {
  security_group_id = module.launch_pad.primary_security_group_id

  type      = "egress"
  from_port = 443
  to_port   = 443
  protocol  = "tcp"

  cidr_blocks = ["0.0.0.0/0"]
}

# ingress

resource "aws_security_group_rule" "allow_ssh_from_everywhere" {
  security_group_id = module.launch_pad.primary_security_group_id

  type      = "ingress"
  from_port = 22
  to_port   = 22
  protocol  = "tcp"

  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "allow_http_from_internal" {
  security_group_id = module.launch_pad.primary_security_group_id

  type      = "ingress"
  from_port = 80
  to_port   = 80
  protocol  = "tcp"

  cidr_blocks = local.internal_subnet_cidr_blocks
}

resource "aws_security_group_rule" "allow_https_from_internal" {
  security_group_id = module.launch_pad.primary_security_group_id

  type      = "ingress"
  from_port = 443
  to_port   = 443
  protocol  = "tcp"

  cidr_blocks = local.internal_subnet_cidr_blocks
}

# bastion role

resource "aws_security_group_rule" "allow_ssh_to_internal" {
  count             = var.enable_bastion_role ? 1 : 0
  security_group_id = module.launch_pad.primary_security_group_id

  type      = "egress"
  from_port = 22
  to_port   = 22
  protocol  = "tcp"

  cidr_blocks = local.internal_subnet_cidr_blocks
}

resource "aws_security_group" "admin_access" {
  count = var.enable_bastion_role ? 1 : 0

  name        = "${var.environment}-admin-access"
  description = "Allows the bastion to access resources associated with this security group."
  vpc_id      = module.vpc_base.vpc_id

  tags = merge(module.launch_pad.tags, {
    "Name" = "${var.environment}-admin-access"
  })
}

resource "aws_security_group_rule" "allow_ssh_from_bastion" {
  count             = var.enable_bastion_role ? 1 : 0
  security_group_id = aws_security_group.admin_access[0].id

  type      = "ingress"
  from_port = 22
  to_port   = 22
  protocol  = "tcp"

  source_security_group_id = module.launch_pad.primary_security_group_id
}
