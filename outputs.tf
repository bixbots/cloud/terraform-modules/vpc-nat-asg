# base
output "vpc_id" {
  description = "The ID of the VPC."
  value       = module.vpc_base.vpc_id
}
output "default_security_group_id" {
  description = "The ID of the default security group."
  value       = module.vpc_base.default_security_group_id
}
output "gateway_id" {
  description = "The ID of the internet gateway."
  value       = module.vpc_base.gateway_id
}
output "public_subnets" {
  description = "A list containing the ID of public subnets."
  value       = module.vpc_base.public_subnets
}
output "private_subnets" {
  description = "A list containing the ID of private subnets."
  value       = module.vpc_base.private_subnets
}
output "data_subnets" {
  description = "A list containing the ID of data subnets."
  value       = module.vpc_base.data_subnets
}
output "public_route_table_id" {
  description = "The ID of the public routing table."
  value       = module.vpc_base.public_route_table_id
}
output "private_route_table_ids" {
  description = "A list containing the ID of private routing tables."
  value       = module.vpc_base.private_route_table_ids
}
output "data_route_table_ids" {
  description = "A list containing the ID of data routing tables."
  value       = module.vpc_base.data_route_table_ids
}

# iam_role
output "nat_iam_role_id" {
  description = "The ID of the NAT IAM role."
  value       = module.launch_pad.iam_role_id
}
output "nat_iam_role_arn" {
  description = "The ARN of the NAT IAM role."
  value       = module.launch_pad.iam_role_arn
}
output "nat_iam_role_name" {
  description = "The name of the NAT IAM role."
  value       = module.launch_pad.iam_role_name
}

# security_group
output "nat_security_group_id" {
  description = "The ID of the NAT security group."
  value       = module.launch_pad.primary_security_group_id
}
output "nat_security_group_arn" {
  description = "The ARN of the NAT security group."
  value       = module.launch_pad.primary_security_group_arn
}
output "nat_security_group_name" {
  description = "The name of the NAT security group."
  value       = module.launch_pad.primary_security_group_name
}

# launch_configuration
output "nat_launch_configuration_id" {
  description = "The ID of the NAT launch configuration."
  value       = aws_launch_configuration.primary.id
}
output "nat_launch_configuration_arn" {
  description = "The ARN of the NAT launch configuration."
  value       = aws_launch_configuration.primary.arn
}
output "nat_launch_configuration_name" {
  description = "The name of the NAT launch configuration."
  value       = aws_launch_configuration.primary.name
}

# autoscaling_group
output "nat_autoscaling_group_ids" {
  description = "The ID of the NAT autoscaling groups."
  value       = aws_autoscaling_group.primary.*.id
}
output "nat_autoscaling_group_arns" {
  description = "The ARN of the NAT autoscaling groups."
  value       = aws_autoscaling_group.primary.*.arn
}
output "nat_autoscaling_group_names" {
  description = "The name of the NAT autoscaling groups."
  value       = aws_autoscaling_group.primary.*.name
}

# tags
output "tags" {
  description = "A combined list of tags including input tags and tags created by this module."
  value       = module.vpc_base.tags
}
