variable "application" {
  type        = string
  default     = "core"
  description = "Name of the application the resource(s) will be a part of."
}

variable "environment" {
  type        = string
  description = "Name of the environment the resource(s) will be a part of. This will be the simple name (i.e. short name) of the VPC."
}

variable "role" {
  type        = string
  default     = "network"
  description = "Name of the role the resource(s) will be performing."
}

variable "zone_id" {
  type        = string
  description = "Specifies the Route53 Zone ID that this VPC belongs to."
}

variable "cidr" {
  type        = string
  description = "CIDR of VPC. All subnets must be inside of this CIDR."
}

variable "public_subnets" {
  type        = list(string)
  description = "A list of public subnets inside the VPC. The subnets will be placed into availability zones in they appear (first subnet goes into the first AZ, second goes into the second, etc)."
}

variable "private_subnets" {
  type        = list(string)
  description = "A list of private subnets inside the VPC. The subnets will be placed into availability zones in they appear (first subnet goes into the first AZ, second goes into the second, etc)."
}

variable "data_subnets" {
  type        = list(string)
  description = "A list of data subnets inside the VPC. The subnets will be placed into availability zones in they appear (first subnet goes into the first AZ, second goes into the second, etc)."
}

variable "azs" {
  type        = list(string)
  description = "A list of Availability zones in the region."
}

variable "enable_dns_hostnames" {
  type        = bool
  default     = true
  description = "(Optional; Default: true) Specifies whether to enable DNS hostnames in the VPC."
}

variable "enable_dns_support" {
  type        = bool
  default     = true
  description = "(Optional; Default: true) Specifies whether to enable DNS support in the VPC."
}

variable "map_public_ip_on_launch" {
  type        = bool
  default     = true
  description = "(Optional; Default: true) Specifies whether to auto-assign public IPs when launching instances in public subnets."
}

variable "enable_bastion_role" {
  type        = bool
  default     = false
  description = "(Optional; Default: false) Specifies whether to configure the NAT instances to also act as a jump server (aka bastion) to the internal (private and data) subnets."
}

variable "ami_id" {
  type        = string
  default     = ""
  description = "(Optional) The ID of the AMI to use for the NAT instances. If not specified, uses the latest version for AWS Linux 2."
}

variable "instance_type" {
  type        = string
  default     = "t3.nano"
  description = "(Optional; Default: t3.nano) The type of EC2 instance to use. If the instance type changes, a new launch configuration will be created and added to the ASG."
}

variable "spot_price" {
  type        = string
  default     = ""
  description = "(Optional; Default: On-demand price) The maximum price to use for reserving spot instances. If omitted then spot instance will not be used."
}

variable "key_name" {
  type        = string
  description = "The name of the SSH key pair that is used for administrator access to the NAT instance."
}

variable "default_cooldown" {
  type        = number
  default     = 300
  description = "(Optional; Default: 300) Amount of time, in seconds, after a scaling activity completes before another can begin."
}

variable "health_check_grace_period" {
  type        = number
  default     = 300
  description = "(Optional; Default: 300) Amount of time, in seconds, after a instance comes into service before checking the health of the instance."
}

variable "enable_monitoring" {
  type        = bool
  default     = false
  description = "(Optional; Default: false) Whether to enable detailed monitoring with launch instances."
}

variable "root_volume_size" {
  type        = number
  default     = 8
  description = "(Optional; Default: 8) The size of the root volume in gigabytes."
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "(Optional) Additional tags to attach to all resources created by this module."
}
