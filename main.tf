module "tags_base" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/tags-base.git?ref=v1"

  application = var.application
  environment = var.environment
  lifespan    = "permanent"
  tags        = var.tags
}

module "vpc_base" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/vpc-base.git?ref=v1"

  application = var.application
  environment = var.environment

  zone_id = var.zone_id

  cidr            = var.cidr
  public_subnets  = var.public_subnets
  private_subnets = var.private_subnets
  data_subnets    = var.data_subnets
  azs             = var.azs

  enable_dns_hostnames    = var.enable_dns_hostnames
  enable_dns_support      = var.enable_dns_support
  map_public_ip_on_launch = var.map_public_ip_on_launch

  tags = module.tags_base.tags
}

module "launch_pad" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/launch-pad.git//core?ref=v1"

  application = var.application
  environment = var.environment
  role        = "${var.role}-nat"

  vpc_id              = module.vpc_base.vpc_id
  enable_admin_access = false
  allow_all_egress    = false

  tags = module.vpc_base.tags
}

data "template_cloudinit_config" "cloudinit_config" {
  gzip          = true
  base64_encode = true

  part {
    filename     = "update-dns.sh"
    content_type = "text/x-shellscript"
    content = templatefile("${path.module}/update-dns.sh", {
      dns_zone_id   = data.aws_route53_zone.primary.zone_id
      dns_zone_name = data.aws_route53_zone.primary.name
    })
  }

  part {
    filename     = "configure-nat.sh"
    content_type = "text/x-shellscript"
    content = templatefile("${path.module}/configure-nat.sh", {
      region = data.aws_region.current.name
      vpc_id = module.vpc_base.vpc_id
    })
  }
}

resource "aws_launch_configuration" "primary" {
  name_prefix   = module.launch_pad.name_prefix
  image_id      = coalesce(var.ami_id, data.aws_ami.primary.id)
  instance_type = var.instance_type
  spot_price    = var.spot_price

  key_name                    = var.key_name
  enable_monitoring           = var.enable_monitoring
  security_groups             = module.launch_pad.security_group_ids
  iam_instance_profile        = module.launch_pad.iam_instance_profile_id
  associate_public_ip_address = true
  user_data_base64            = data.template_cloudinit_config.cloudinit_config.rendered

  root_block_device {
    volume_type           = "gp3"
    delete_on_termination = true
    volume_size           = var.root_volume_size
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "primary" {
  count = length(var.azs)

  name                 = "${aws_launch_configuration.primary.name}-${var.azs[count.index]}"
  vpc_zone_identifier  = [module.vpc_base.public_subnets[count.index]]
  launch_configuration = aws_launch_configuration.primary.id

  min_size = 1
  max_size = 1

  default_cooldown          = var.default_cooldown
  health_check_grace_period = var.health_check_grace_period
  health_check_type         = "EC2"

  termination_policies = [
    "OldestLaunchConfiguration",
    "ClosestToNextInstanceHour"
  ]

  dynamic "tag" {
    for_each = module.launch_pad.tags
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }

  tag {
    key                 = "Name"
    value               = "${module.launch_pad.name}-${var.azs[count.index]}"
    propagate_at_launch = true
  }

  lifecycle {
    create_before_destroy = true
  }
}
