data "aws_iam_policy_document" "route53" {
  statement {
    actions = [
      "route53:GetHostedZone",
      "route53:ListResourceRecordSets",
      "route53:ChangeResourceRecordSets",
    ]
    resources = ["arn:aws:route53:::hostedzone/${data.aws_route53_zone.primary.zone_id}"]
  }

  statement {
    actions = [
      "route53:ListHostedZones",
      "route53:ListHostedZonesByName",
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "route53" {
  name   = "${module.launch_pad.name}-update-dns"
  policy = data.aws_iam_policy_document.route53.json
}

resource "aws_iam_role_policy_attachment" "route53" {
  policy_arn = aws_iam_policy.route53.arn
  role       = module.launch_pad.iam_role_name
}

data "aws_iam_policy_document" "primary" {
  statement {
    actions = [
      "ec2:CreateRoute",
      "ec2:DeleteRoute",
      "ec2:ModifyInstanceAttribute",
      "ec2:ReplaceRoute",
      "ec2:DescribeRouteTables",
      "ec2:Describe*"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "primary" {
  name   = module.launch_pad.name
  policy = data.aws_iam_policy_document.primary.json
}

resource "aws_iam_role_policy_attachment" "primary" {
  policy_arn = aws_iam_policy.primary.arn
  role       = module.launch_pad.iam_role_name
}
