#!/usr/bin/env bash

sudo yum update -y
sudo yum install -y jq

export AWS_DEFAULT_REGION=$(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | jq -r .region)
AVAILABILITY_ZONE=$(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone)
INSTANCE_ID=$(curl -s http://169.254.169.254/latest/meta-data/instance-id)
VPC_ID="${vpc_id}"

aws ec2 modify-instance-attribute --no-source-dest-check --instance-id $INSTANCE_ID

# Determine route tables that need to use NAT in the current availability zone
ROUTE_TABLES=$(aws ec2 describe-route-tables --query "RouteTables[?VpcId=='$VPC_ID'&&Tags[?Key=='AvailabilityZone'&&Value=='$AVAILABILITY_ZONE']].RouteTableId" --output text | sed $'s/\t/\\\n/g')

for ROUTE_TABLE in $ROUTE_TABLES
do
	STATE=$(aws ec2 describe-route-tables --route-table-ids "$ROUTE_TABLE" --output text --query "RouteTables[0].Routes[?DestinationCidrBlock=='0.0.0.0/0'] | [0].State")
	TARGET=$(aws ec2 describe-route-tables --route-table-ids "$ROUTE_TABLE" --output text --query "RouteTables[0].Routes[?DestinationCidrBlock=='0.0.0.0/0'] | [0].InstanceId")

	echo "Checking $ROUTE_TABLE"
	if [ "$STATE" == "blackhole" ]; then
		# Replace default route
		echo "Default route is invalid. Replacing default route to $INSTANCE_ID"
		aws ec2 replace-route --route-table-id "$ROUTE_TABLE" --destination-cidr-block "0.0.0.0/0" --instance-id "$INSTANCE_ID"
	elif [ "$TARGET" == "None" ]; then
		# Create default route
		echo "No default route is detected. Creating default route to $INSTANCE_ID"
		aws ec2 create-route --route-table-id "$ROUTE_TABLE" --destination-cidr-block "0.0.0.0/0" --instance-id "$INSTANCE_ID"
	elif [ "$TARGET" != "$INSTANCE_ID" ]; then
		# Replace default route
		echo "Default route is invalid. Replacing default route to $INSTANCE_ID"
		aws ec2 replace-route --route-table-id "$ROUTE_TABLE" --destination-cidr-block "0.0.0.0/0" --instance-id "$INSTANCE_ID"
	else
		echo "No change is required"
	fi

done
